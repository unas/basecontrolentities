package com.unasnet.basecontrol.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Base")
public class Base {
	
	@Id
	@Column(name="ID")
	private String ID;
	@Column(name="Name")
	private String Name;
	@Column(name="ZoneID")
	private String ZoneID;
	@Column(name="GPSLat")
	private Double GPSLat;
	@Column(name="GPSLon")
	private Double GPSLon;
	@Column(name="TerminalTractorID")
	private String TerminalTractorID;
	
	public Base() {
		
	}
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getZoneID() {
		return ZoneID;
	}
	public void setZoneID(String zoneID) {
		ZoneID = zoneID;
	}
	public Double getGPSLat() {
		return GPSLat;
	}
	public void setGPSLat(Double gPSLat) {
		GPSLat = gPSLat;
	}
	public Double getGPSLon() {
		return GPSLon;
	}
	public void setGPSLon(Double gPSLon) {
		GPSLon = gPSLon;
	}
	public String getTerminalTractorID() {
		return TerminalTractorID;
	}
	public void setTerminalTractorID(String terminalTractorID) {
		TerminalTractorID = terminalTractorID;
	}
}
