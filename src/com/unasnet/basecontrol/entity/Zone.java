package com.unasnet.basecontrol.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Zone")
public class Zone {

	@Id
	@Column(name="ID")
	private String ID;
	@Column(name="Name")
	private String Name;
	
	public Zone() {
		
	}
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
}
